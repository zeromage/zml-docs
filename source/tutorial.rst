========
Tutorial
========


First steps
-----------

Create a file example.py:

::

    import zml

    class User(object):
        def __init__(self):
            self.active = True

    user1 = User()
    user1.firstname = 'Richard'
    user1.lastname = 'Langly'
    user1.email = 'ringo@l4ngly.org'

    user2 = User()
    user2.firstname = 'Melvin'
    user2.lastname = 'Frohike'
    user2.email = 'melvin@frohike1.net'

    user3 = User()
    user3.firstname = 'John Fitzgerald'
    user3.lastname = 'Byers'
    user3.email = 'jfb@byers23.org'

    context = dict()
    context['users'] = [user1, user2, user3]
    context['styles'] = ['main.css', 'more.css']

    html = zml.render('page.zml', context)
    print(html)


Create a file page.zml:

:: 

    %inherit 2col

    *col1_content:
      div.panel:
        %for user in users:
          h1: 'User'
          div.card:
            %if user.active:
              p: '{user.firstname}'
              p: '{user.lastname}'
              p: '{user.email}'
            %else:
              p: 'The user is not active'

Create a file 2col.zml:

::

    html:
      head:
        title: 'ZML'
      body:
        h1: 'zml - zero markup language'
        div.grid:
          div.m66:
            div.left: '{*col1_content}'
          div.m33:
            div.right: 'some sidebar stuff'


Start the example script:

::

    python3 example.py


Examples
--------

You can find some examples in the folder zml/examples.
Have a look at the github repository: https://gitlab.com/zeromage/zml-py

::

    git clone git@gitlab.com:zeromage/zml-py.git


