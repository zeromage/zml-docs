============
Installation
============

Simple installation
-------------------

Install the python3 module with pip

::

    pip3 install zml


You are welcome to contribute to the project. The project's git repository can be found at https://gitlab.com/zeromage/zml-py



Developer setup
---------------

This section is helpful if you want to contribute to the zml project.


Install packages
~~~~~~~~~~~~~~~~

::

  sudo dnf install zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel xz xz-devel libffi-devel

  or

  sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \


Install geckodriver
~~~~~~~~~~~~~~~~~~~

::

  wget https://github.com/mozilla/geckodriver/releases/download/v0.23.0/geckodriver-v0.23.0-linux64.tar.gz
  tar xzvf geckodriver-v0.23.0-linux64.tar.gz
  cp geckodriver /usr/local/bin
  sudo chmod ug+x /usr/local/bin/geckodriver


Setup pyenv
~~~~~~~~~~~

::

  git clone https://github.com/pyenv/pyenv.git ~/.pyenv
  echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
  echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
  echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile
  exec "$SHELL"

Zsh note: Modify your ~/.zshenv file instead of ~/.bash_profile. Ubuntu and Fedora note: Modify your ~/.bashrc file instead of ~/.bash_profile.


Setup pyenv-virtualenv
~~~~~~~~~~~~~~~~~~~~~~

::

  git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
  exec "$SHELL"

Zsh note: Modify your ~/.zshenv file instead of ~/.bash_profile


Install pyenv-virtualenvwrapper
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  git clone https://github.com/pyenv/pyenv-virtualenvwrapper.git $(pyenv root)/plugins/pyenv-virtualenvwrapper
  echo 'export WORKON_HOME=~/.pyenv/versions' >> ~/.bash_profile

Zsh note: Modify your ~/.zshrc file instead of ~/.bash_profile. Ubuntu and Fedora note: Modify your ~/.bashrc file instead of ~/.bash_profile.


Setup zml development environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  mkdir -p ~/git/zml
  cd ~/git/zml
  git clone git@gitlab.com:zeromage/zml-py.git .
  pyenv install 3.6.6
  mkvirtualenv zml-py -ppython3
  workon zml-py
  setvirtualenvproject 
  toggleglobalsitepackages (if needed)
  sudo ln -s ~/git/zml-py/zml ~/.venvs/zml-py/lib/python3.6/site-packages/zml
  `which pip` install pip-tools
  `which pip` install -r requirements/dev.txt
  python3 functional_tests.py
