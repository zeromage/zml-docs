Development
***********

Implementations
===============

Python implementation
---------------------

Development repositories
^^^^^^^^^^^^^^^^^^^^^^^^

https://gitlab.com/zeromage/zml-docs
https://gitlab.com/zeromage/zml-py


Javascript implementation
-------------------------
The development repository for the javascript implementation will be listed here.

Ruby implementation
-------------------
The development repository for the ruby implementation will be listed here.


PHP implementation
-------------------
The development repository for the PHP implementation will be listed here.


The ZML Working Group encourages the community driven creation of implementations for the different programming languages and web development frameworks.


Contact to the ZML Working Group
================================

Contact: team(at)zml.org
