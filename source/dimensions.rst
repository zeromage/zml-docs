Dimensions
==========

Internal Relations
------------------

Definition in ZML:

key: value

f.e.

firstname: 'Melvin'

will be parsed like this:
firstname will be transformed into a dimension(=property).

dimension could be an external defined dimension:
f.e.

org.zml.firstname: 'Melvin'

A view will then just define the dimensions to display:
f.e.

::

  *view1:
    dimensions:
      - org.zml.firstname
      - org.zml.lastname
      - org.zml.email

External defined types
----------------------

But also external defined types and relations to external defined objects are possible:

cell1: cell2

::

    $person_1:
      org.zml.father: person_2
      org.zml.mother: person_3
      org.zml.husband: stricker.com/person_4
      org.zml.daughter: person_5
      org.zml.son: person_6
      org.zml.children:
        - person_5
        - person_6

    $person_2:
      org.zml.daughter: person_1

    $person_3:
      org.zml.daughter: person_1

    $person_5:
      org.zml.mother: person_1

    $person_6:
      org.zml.mother: person_1


Note also the multiple usage of person_1, person_5, and person_6, which are references to the original cell $person_1.


The external server provides also a person model:

::

    !en:
      labels:
        title: 'Title'
        date: 'Date'
        bodytext: 'Bodytext'
      buttons:
        save: 'Save'
    !de:
      labels:
        title: 'Titel'
        date: 'Datum'
        bodytext: 'Haupttext'
      buttons:
        save: 'Speichern'

    +person:
      first_name:
        @type: str
        @label: !labels.title
      father:
        @type: person
      mother:
        @type: person 

    *father(person):
      children:
        @type: person
        @container: list

    *mother(person):
      children:
        @type: person
        @container: list

    +wife(person):
      husband:
        @type: person

    +husband(person)
      wife:
        @type: person


The models wife and husband extend the person model with properties.
The referencing app can then use this model:

::

    $person_1: +person()
      first_name: 'Katja'
      father: person_2
      mother: person_3
      husband: person_4
      daughter: person_5
      son: person_6
      children:
        - person_5
        - person_6

    $person_2: +person()
      name: 'Karlheinz'
      daughter: person_1

    $person_3: +person()
      name: 'Rotraud'
      daughter: person_1

    $person_4: +person()
      name: 'Christof'
      wife: person_1

    $person_5: +person()
      name: 'Mila'
      mother: person_1

    $person_6: +person()
      name: 'Alva'
      mother: person_1



Relations to external defined objects
-------------------------------------


::

    $person_1:
      org.zml.husband: stricker.com/person_4(QmYPNmahJAvkMTU6tDx5zvhEkoLzEFeTDz6azDCSNqzKkW)

User4 is then defined on an external server: f.e. stricker.com

::

    $person_4:
      org.zml.wife: person_1

This implies that stricker.com/person_4 is persisted immutable on the global file system.
The current implementation use IPFS (InterPlanetary File System) to persist the objects in an immutable way, by using
versioning on objects (and/or documents).
The content id (CID) QmYPNmahJAvkMTU6tDx5zvhEkoLzEFeTDz6azDCSNqzKkW is used to identify the exact version of the object.
See https://github.com/multiformats/cid



Undefined properties
--------------------

In the real world there are spontaneous taggings of relations of object to other objects.
F.e. there is a new task of the management that all orders should be searchable by the chat operator, who initialized the order.
(f.e. service person in website chat helping a website user to find the right product)

The order model has no property 'chat_operator', and we dont want to add it to the model.

::

    +order:
      title:
        @type: str
      price:
        @type: int

    $person_6:
     first_name: 'Bob'

    order_1: +order()
      title: 'Great product 1'
      price: 120
      chat_operator: person_6


Relations to external defined objects
-------------------------------------


::

    $person_1:
      org.zml.husband: stricker.com/person_4(QmYPNmahJAvkMTU6tDx5zvhEkoLzEFeTDz6azDCSNqzKkW)

User4 is then defined on an external server: f.e. stricker.com

::

    $person_4:
      org.zml.wife: person_1

This implies that stricker.com/person_4 is persisted immutable on the global file system.
The current implementation use IPFS (InterPlanetary File System) to persist the objects in an immutable way, by using
versioning on objects (and/or documents).

  price: 120
  chat_operator: person_6


A new applitude could use chat_operator property without the need for a programmer to extend models or views.
This is a new programming concept compared to object orientied programming, where every object has to be hierarhical and predefined.
In OO-Programming the developers have to change models, change views, transform data from one model to another model to allow cross application usage of data objects.
In user-oriented programming (UOP) the user defined the views on the fly without the need to specify models. Of course models ARE used
in a minimalistic and unobtrusive way (just the minimal necessary models to give the user some basis for data exploration and automatic form creation).

Another example:

::

    +wife(person):
      husband:
        @type: person

    +husband(person)
      wife:
        @type: person

    *father(person):
      children:
        @type: person
        @container: list


The models wife and husband extend the person model with properties.
This makes sense as a wife MUST have a husband and husband MUST have a wife.
Also a father or mother MUST have children.
Other properties f.e. a father having a son or a daughter are optional as a father might have a son, but he might also have no son but a daughter.
It would then make no sense to have a property "son" if he has a daughter. And vice versa.
Such properties, which are optional will not be defined in models. They are just used on the fly on a data object without requiring a definition of such property in a model.


::

      
    $person_2: +person()
      name: 'Karlheinz'
      children:
        - person_1
      daughter: person_1

Here the property daughter is defined on the fly, as it is not necessary that the father has a daughter, he could have a son.
If we would defined all optional properties in the models the models would get bloated over the time.
Departments can augment the data of the core domain of the company for special use cases witout disturbing the core domain processes and without the need for programmers to 
deploy a new feature.


Linked data
-----------

Linking to internal objects
~~~~~~~~~~~~~~~~~~~~~~~~~~~

$user.somepropertyisinternalobject#start=301,length=30


Linking to external objects
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Linking to external documents in ZML is done by using transclusion with the following syntax:

$user.somepropertyisexternalobject#start=301,length=30


Convention: Properties are dimensions
-------------------------------------

Draft:

In ZML a property must be defined as a prefixed dimension.
The prefix is the domain of the definition. The dimension is described at the api endpoint of the dimension definition.
A dimension definition may include meta-data for transforming to other dimensions and views.

It is a strong requirement to not us arbitrary descriptors for properties but to use defined dimensions as properties in order
to allow dimension-sensitive functions and views when following or handling properties of objects.

F.e. calling a telephone number, when klicking on a org.zml.phone property.


ZZ-structures
-------------

The tree-like structure of a zml-document is a subset of a zz-structure.
ZML-documents can be described by a zz-structure, which means that an implementation can parse the ZML document and display it as a zz-structure.


The golden ZML convention
-------------------------
"Properties are nodes with two-way connections."
If in ZML the convention is to interpret the key-value constructs of a ZML line as a two-way connection between two cells, we
allow a lean syntax of the ZML-document to describe a richer structure of zz-structures.

In fact ZML can provide the most lean format for a human readable document format for zz-structures, because the two sides of a link are defined
implicitly.

Pros of the golden convenvention:

The property is aware of the related object. F.e. a property "subcompany" is aware of the relation to the company object using the "subcompany" property.
Indeed the "subcompany" property/cell is aware of all relations to other objects.

For this to be true, there must be an id for every property. As its not possible with the current syntax to define ids for simple properties, but only
for object properties, we must invent a syntax for defining ids on simple properties or live with breaking the golden convention.

::

    $person_1: +person()
      first_name: 'Katja'
      father: person_2

    $philosopher: +profession()
      title: 'Philosopher'

    $person_2: +person()
      name: 'Karlheinz'
      daughter: person_1
      profession: philosopher

F.e. in this ZML document "person_1" is the id/alias of the object, so the golden rule is not broken.
The profession property uses a separated definition with the descriptor "philosopher", which is the alias for the object id.
The id is constructed by the document address the object is defined in concatenated with the descriptor:
f.e. 

Exchanging $ with # for the data definitions, we get a more expressive glyph for transforming model (+) to instance (#) and
also a more commong symbol for anchor points in documents (comparable to anchors in url schemes):

com.somedomain/somedocument?v=1.0#person_1
or for a domainwide data store:

com.somedomain?v=1.0#person_1

Also # is a common symbol for number (id).

::

    #person_1: +person()
      first_name: 'Katja'
      father: #person_2

    #philosopher: +profession()
      title: 'Philosopher'

    #person_2: +person()
      name: 'Karlheinz'
      daughter: #person_1
      profession: #philosopher

Properties can be accessed by using dot notation:

com.somedomain?v=1.0#person_1.first_name

Example for accessing external defined profession:

Definition of philosopher object at com.somecompany:

::

    #philosopher: +profession()
      title: 'Philosopher'

Relation to the external philosopher object:

::

    #person_1: +person()
      first_name: 'Katja'
      father: #person_2

    #person_2: +person()
      name: 'Karlheinz'
      daughter: #person_1
      profession: com.somecompany?v=1.0#philosopher

Alternatively we can use a proxy object (clone):

::

    #person_1: +person()
      first_name: 'Katja'
      father: #person_2

    #philosopher: com.somecompany?v=1.0#philosopher

    #person_2: +person()
      name: 'Karlheinz'
      daughter: #person_1
      profession: #philosopher

External definition of model at somecompany.com:

::

    +person:
      first_name:
        @type: str
        @label: !labels.title
      father:
        @type: person
      mother:
        @type: person 


Reference to external model:

::

  #person_1: com.somecompany?v1.0+person()
      first_name: 'Katja'
      father: #person_2
    

A document has in ZML has no fixed model associated. It only defines the properties of the entity.
The models are only used by other document, which reference the entity.
Models are more like intepretations, there are no canonical fixed models associated with one entity.


