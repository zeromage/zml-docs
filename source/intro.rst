=========
About ZML
=========

Overview
--------

The Zero Markup Language, abbreviated ZML, is a template language to represent hierarchically structured data in conjunction with template logic in an integrated system. Structuring is without wrapping in <tags></tags> using indentation (indentation). ZML is designed for a platform- and implementation-independent use. A very lean but powerful syntax is based on glyphs:

::

    * Views
    + Models
    % Logic
    ! Translation
    # Data
    & Metadata
    @ Resources
    ~ Routes
    | Slots
    < Signal handler
    > Signal emitter
    " Comment

The language uses a MVS approach. (Model View Signal)

::

    %import components
    %inherit 2col

    *col1_content:
      div.panel:
        %for user in users:
          h1: User 
          div.card:
            %if user.active:
              p: {user.firstname}
              p: {user.lastname}
              p: {user.email}
            %else:
              p: The user is not active

This ZML template produces the following HTML code:

::

    <!DOCTYPE html>
    <html>
      <head>
        <title>zml</title>
      </head>
      <body>
        <h1>zml  -  zero  markup  language</h1>
        <div class="grid" >
          <div class="m66" >
            <div class="left" >
              <div class="panel" >
                <h1>User</h1>
                <div class="card" >
                  <p>Richard</p>
                  <p>Langly</p>
                  <p>ringo@l4ngly.org</p>
                </div>
                <h1>User  </h1>
                <div class="card" >
                  <p>Melvin</p>
                  <p>Frohike</p>
                  <p>melvin@frohike1.net</p>
                </div>
                <h1>User  </h1>
                <div class="card" >
                  <p>John Fitzgerald</p>
                  <p>Byers</p>
                  <p>jfb@byers23.org</p>
                </div>
              </div>
            </div>
          </div>
          <div class="m33" >
            <div class="right" >some  sidebar  stuff</div>
          </div>
        </div>
      </body>
    </html>


