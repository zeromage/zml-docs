.. zero markup language documentation master file, created by
   sphinx-quickstart on Fri Dec 14 00:57:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to zero markup language's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   spec/0.3/spec
   install
   tutorial
   development


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
